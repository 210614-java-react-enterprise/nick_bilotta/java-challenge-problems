package com.revature.eval.java.core;

import java.time.Month;
import java.time.temporal.Temporal;
import java.util.*;
import java.nio.charset.StandardCharsets;
import java.lang.Math;
import java.time.LocalDateTime;
import java.time.LocalDate;

// This is a test comment

public class EvaluationService {



	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 *
	 * @param string
	 * @return
	 */
	public String reverse(String string) {
		String input = string;

		// Take input and split into array of characters
		byte[] stringBytes = input.getBytes();
		byte[] result = new byte[stringBytes.length];

		// Going in reverse, place each element into a new array
		for (int i = 0; i < stringBytes.length; i++)
			result[i] = stringBytes[stringBytes.length - i - 1];

		// In a new string, append each element of new array
		String reversedString = new String(result, StandardCharsets.UTF_8);

		// Return reversed string
		return reversedString;
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 *
	 * @param phrase
	 * @return
	 */
	public String acronym(String phrase) {

		String solution = "";
		// Store each word as an element in an array
		String[] wordArray = phrase.split(" |-");

		//Take each word in the array and copy the first character
		for (int i = 0; i < wordArray.length; i++) {
			String word = wordArray[i];
			solution += word.charAt(0);
		}
		// Return the new string after setting to uppercase
		return solution.toUpperCase();
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */
	static class Triangle {
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle() {
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			boolean answer = false;
			if (sideOne == sideTwo && sideTwo == sideThree && sideThree == sideOne) {
				answer = true;
			}
			return answer;
		}

		public boolean isIsosceles() {
			boolean answer = false;
			if (sideOne == sideTwo || sideOne == sideThree || sideTwo == sideThree) {
				answer = true;
			}
			return answer;
		}

		public boolean isScalene() {
			boolean answer = false;
			if (sideOne != sideTwo && sideOne != sideThree && sideTwo != sideThree) {
				answer = true;
			}
			return answer;
		}

	}

//	 * 4. Given a word, compute the scrabble score for that word.


	public int getScrabbleScore(String word) {
		// variable storing the total points of a word
		int score = 0;

		// Parse the input word into an array of letters
		word = word.toUpperCase();
		String[] letters = word.split("");

		// Arrays storing the value of each possible letter
		String[] p1 = new String[]{"A", "E", "I", "L", "N", "O", "R", "S", "T", "U"};
		String[] p2 = new String[]{"D", "G"};
		String[] p3 = new String[]{"B", "C", "M", "P"};
		String[] p4 = new String[]{"F", "H", "V", "W", "Y"};
		String[] p5 = new String[]{"K"};
		String[] p8 = new String[]{"J", "X"};
		String[] p10 = new String[]{"Q", "Z"};

		// Array storing each letter array
//		String[][] arrays = {p1, p2, p3, p4, p5, p8, p10};

		// Iterate over the letters array
		for (String letter : letters) {
			// Iterate each letter over the point value arrays
			if (checkLetter(letter, p1)) {
				score += 1;
			} else if (checkLetter(letter, p2)) {
				score += 2;
			} else if (checkLetter(letter, p3)) {
				score += 3;
			} else if (checkLetter(letter, p4)) {
				score += 4;
			} else if (checkLetter(letter, p5)) {
				score += 5;
			} else if (checkLetter(letter, p8)) {
				score += 8;
			} else if (checkLetter(letter, p10)) {
				score += 10;
			}

		}

		return score;
	}

	public boolean checkLetter(String letter, String[] arr){
		// This method checks whether a character is in the given array
		boolean inArr = false;
		// iterates over the array and checks the equivalence of values
		for (String s : arr) {
			if (letter.equals(s)) {
				inArr = true;
				break;
				// loop terminates early if a match is found
			}
		}
		return inArr;
	}

	/*
	 Points Per Letter:

	 0 Points - Blank tile.
	 1 Point - A, E, I, L, N, O, R, S, T and U.
	 2 Points - D and G.
	 3 Points - B, C, M and P.
	 4 Points - F, H, V, W and Y.
	 5 Points - K.
	 8 Points - J and X.
	 10 Points - Q and Z
	*/



	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 *
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 *
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 *
	 * The format is usually represented as
	 *
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 *
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 *
	 * For example, the inputs
	 *
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 *
	 * 6139950253
	 *
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 */
	public String cleanPhoneNumber(String string) {

		String newNum = string.replaceAll("\\s+", "");
		newNum = newNum.replaceAll("\\-|\\(|\\)|\\.", "");

		if (newNum.length() > 11) {
			throw new IllegalArgumentException();
		}

		// Validate number format
		try {
			long number = Long.parseLong(newNum);
		} catch (NumberFormatException e){
			throw new IllegalArgumentException();
		}

		return newNum;
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 *
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 *
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String string) {
		Map<String, Integer> wordCounter = new HashMap<>();

		// Splitting the words of string based on spaces, commas, and line-breaks
		string = string.replace("\n", "");
		String[] words = string.split(" |,");

		for (String word : words) {
			// merge the word into the map, if present, increment value
			wordCounter.merge(word, 1, Integer::sum);
		}

		return wordCounter;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	static class BinarySearch<T extends Comparable> {
		private List<T> sortedList;

		public int indexOf(T t) {
			// TODO Write an implementation for this method declaration
			int low = 0;
			int high = sortedList.size()-1;
			int mid;
			int count = 0;

			if (high>=low){

				if ((high-low)%2==0) {
					mid = (high-low)/2;
				} else {
					mid = (((high-low)/2)+(high-low)%2);
				}

				while (sortedList.get(mid).compareTo(t)!=0) {
					System.out.println("start of while loop");
					//if value at index=mid > t, then t is in left half of array
					if (sortedList.get(mid).compareTo(t)>0) {
						//Search the left half, reset high to the old mid
						high = mid;
						System.out.println("Setting new high to: "+high);
						mid = ((high-low)/2);
						System.out.println("Setting new mid to:"+mid);
						System.out.println("attempt to search left half");
					} else if (sortedList.get(mid).compareTo(t)<0) {
						//This means that t is in the right half
						low = mid;
						System.out.println("Setting new low to: "+low);
						mid += (((high-low)/2)+(high-low)%2);
						System.out.println("Setting new mid to:"+mid);
						System.out.println("attempt to search right half");
					} else if (sortedList.get(mid)==t){
						System.out.println("Assert equals");
						return mid;
					} else {
						System.out.println("Comparisons failed");
					}
					count++;
					if (count>10) {
						break;
					}

				}
				return mid;
			}
			return 0;
		}

		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}

	}

	/**
	 * 8. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		// TODO Write an implementation for this method declaration
		String[] vowels = new String[]{"a", "e", "i", "o", "u"};
		String answer = "";
		String[] words = string.split(" ");

		for (int i = 0; i < words.length; i++) {
			//This level loops over the array of stored words
			//Turn each letter into an array element
			String[] letters = words[i].split("");
			//Determine if the first letter is in vowels
			boolean isVowel = false;
			for (int v = 0; v<vowels.length; v++) {
				if (letters[0].equals(vowels[v])){
					isVowel = true;
				}
			}

			//Now rearrange the word based on if index 0 is a vowel
			if (isVowel) {
				//Simply add 'ay' to the end
				String newWord = words[i].concat("ay");

				//checks if answer is empty, if not, it adds a space to separate multiple words
				if (!answer.equals("")) {
					answer = answer.concat(" ");
				}
				answer = answer.concat(newWord);


			} else {
				//Also have to test for 'sch' and 'th'
				if (letters[0].equals("s") && letters[1].equals("c") && letters[2].equals("h")) {
					//Move SCH to the end then add AY
					String newWord = "";
					for (int l = 3; l < letters.length; l++) {
						newWord = newWord.concat(letters[l]);
					}
					newWord = newWord.concat(letters[0]+letters[1]+letters[2]+"ay");

					if (!answer.equals("")) {
						answer = answer.concat(" ");
					}
					answer = answer.concat(newWord);


				} else if (letters[0].equals("t") && letters[1].equals("h")) {
					//Move TH to the end and then add AY
					String newWord = "";
					for (int l = 2; l < letters.length; l++) {
						newWord = newWord.concat(letters[l]);
					}
					newWord = newWord.concat(letters[0]+letters[1]+"ay");

					if (!answer.equals("")) {
						answer = answer.concat(" ");
					}
					answer = answer.concat(newWord);


				} else if (letters[0].equals("q") && letters[1].equals("u")) {
					//Move QU to the end
					String newWord = "";
					for (int l = 2; l < letters.length; l++) {
						newWord = newWord.concat(letters[l]);
					}
					newWord = newWord.concat(letters[0]+letters[1]+"ay");

					if (!answer.equals("")) {
						answer = answer.concat(" ");
					}
					answer = answer.concat(newWord);


				} else {
					//Move first letter to the end and then add ay
					String newWord = "";
					for (int l = 1; l < letters.length; l++) {
						newWord = newWord.concat(letters[l]);
					}
					newWord = newWord.concat(letters[0]+"ay");

					if (!answer.equals("")) {
						answer = answer.concat(" ");
					}
					answer = answer.concat(newWord);

				}
			}
		}


		return answer;
	}

	/**
	 * 9. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		// TODO Write an implementation for this method declaration
		//Convert input integer into an array of digits

		String temp = Integer.toString(input);
		int[] digits = new int[temp.length()];
		for (int i = 0; i < temp.length(); i++) {
			digits[i] = Character.getNumericValue(temp.charAt(i)); ;
		}
		System.out.println(Arrays.toString(digits));
		//Raise each digit to the power of temp.length(), then total the results
		int total = 0;
		double var;

		for (int digit : digits) {
			//Taking the digit at index d, raising to the power of digits.length
			var = Math.pow(digit, digits.length);
			System.out.println("var: "+var);
			total += var;
		}
		System.out.println(total);
		return total == input;
	}

	/**
	 * 10. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		String answerString = "";
		while (l%2==0) {
			answerString = answerString.concat(2+" ");
			l /= 2;
		}

		for (int i = 3; i <= Math.sqrt(l); i += 2) {
			// While i divides n, print i and divide n
			while (l % i == 0) {
				answerString = answerString.concat(i + " ");
				l /= i;
			}
		}

		if (l>2) {
			answerString = answerString.concat(l+"");
		}

		String[] stringFactors = answerString.split(" ");

		List<Long> numbers = new ArrayList<>();
		for (String stringFactor : stringFactors) {
			numbers.add(Long.parseLong(stringFactor));
		}


		return numbers;
	}

	/**
	 * 11. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 * 
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 * 
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 * 
	 * A ROT13 on the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 * 
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 * 
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	static class RotationalCipher {
		private int key;

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public String rotate(String string) {
			//Create an array of the valid alphabet characters, lower and uppercase
			String[] alphabetLower = "abcdefghijklmnopqrstuvwxyz".split("");
			String[] alphabetUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
			//Store the input in an array, initialize the output array
			String[] entryArray = string.split("");
			String[] outputArray = new String[string.length()];

			System.out.println(key);


			for (int i = 0; i < string.length(); i++) {
				//At each character in the string, we iterate through the alphabet looking for a match
				for (int a = 0; a < alphabetLower.length; a++) {

					//Check if the character is in either alphabet array
					if (alphabetLower[a].equals(entryArray[i])) {
						//Move the letter based on the key
						if ((a + key) > 25) {
							//Wrap around the array
							outputArray[i] = alphabetLower[(a + key) - 26];

						} else {
							outputArray[i] = alphabetLower[(a + key)];

						}
						break;

					} else if (alphabetUpper[a].equals(entryArray[i])) {
						//Move the letter based on the key
						if ((a + key) > 25) {
							//Wrap around the array
							outputArray[i] = alphabetUpper[(a + key) - 26];

						} else {
							outputArray[i] = alphabetUpper[(a + key)];

						}
						break;

					} else {
						//Character is not a letter, returned as normal
						outputArray[i] = entryArray[i];

					}

				}


			}
			//End of for loop replacing the values
			//Concat every value in the array to a new string
			String output = "";
			for (String s : outputArray) {
				output = output.concat(s);
			}

			return output;
		}

	}

	/**
	 * 12. Given a number n, determine what the nth prime is.
	 * 
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 * 
	 * @param
	 * @return
	 */
	public int calculateNthPrime(int nth) {
		// TODO Write an implementation for this method declaration
		if (nth<1){
			throw new IllegalArgumentException();
		}

		int num, count, i;
		num=1;
		count=0;

		while (count < nth){
			//We count the occurances of primes until it matches the input
			num=num+1;
			for (i = 2; i <= num; i++) {
				//Starting at 2, we check if remainder of num / i is 0
				if (num % i == 0) {
					break;
				}
			}
			//If the first occurance of a zero remainder is itself, the number is  prime
			if ( i == num){//if it is a prime number
				count = count+1;
			}
		}
		//After we reach the desired count of primes, we return the most recent number (the last positive prime)
		return num;
	}

	/**
	 * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {

		/**
		 * Question 13
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			//Set an array for the alphabet and another backwards
			String[] alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
			String[] alphUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
			String[] backward = "zyxwvutsrqponmlkjihgfedcba".split("");
			String[] numbers = "1234567890".split("");
			String[] inputArr = string.split("");
			String output = "";
			int count = 0;

			for (int i = 0; i < inputArr.length; i++) {
				//Loop through the inputArr, store the current character
				String temp = inputArr[i];
				if (Arrays.asList(alphabet).contains(temp)) {
					//For letters, find the position of that letter in the array, return that position in the flipped one
					for (int a = 0; a < alphabet.length; a++) {
						if (alphabet[a].equals(temp)) {
							//when a match is found for the letter, take the index and add the opposite to output
							output = output.concat(backward[a]);
							count++;
							if (count%5==0) {

								output = output.concat(" ");
							}
						}
					} //this is th end of the alphabet scan loop
				} else if (Arrays.asList(alphUpper).contains(temp)) {
					//For letters, find the position of that letter in the array, return that position in the flipped one
					for (int a = 0; a < alphUpper.length; a++) {
						if (alphUpper[a].equals(temp)) {
							//when a match is found for the letter, take the index and add the opposite to output
							output = output.concat(backward[a]);
							count++;
							if (count%5==0) {

								output = output.concat(" ");
							}
						}
					} //this is th end of the alphabet scan loop
				} else if (Arrays.asList(numbers).contains(inputArr[i])) {
					output = output.concat(inputArr[i]);
					count++;
					if (count%5==0) {

						output = output.concat(" ");
					}
				}
			} // end of the loop through the input array
			//Do not return a space at the end of the string
			if (output.substring(output.length() - 1).equals(" ")) {
				//Remove the last space
				output = output.substring(0, output.length() - 1);
			}

			return output;
		}

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			//Set an array for the alphabet and another backwards
			String[] alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
			String[] backward = "zyxwvutsrqponmlkjihgfedcba".split("");
			String[] inputArr = string.split("");
			String[] numbers = "1234567890".split("");
			String output = "";

			for (int i = 0; i < inputArr.length; i++) {
				//Loop through the inputArr, store the current character
				String temp = inputArr[i];
				if (Arrays.asList(alphabet).contains(temp)) {
					//For letters, find the position of that letter in the array, return that position in the flipped one
					for (int a = 0; a < alphabet.length; a++) {
						if (alphabet[a].equals(temp)) {
							//when a match is found for the letter, take the index and add the opposite to output
							output = output.concat(backward[a]);
						}
					} //this is th end of the alphabet scan loop
				} else if (Arrays.asList(numbers).contains(inputArr[i])) {
					output = output.concat(inputArr[i]);
				}
			}


			return output;
		}
	}

	/**
	 * 15. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		//split the input into an array
		String inputClean = string.replace("-","");
		String[] inputArr = inputClean.split("");
		int[] inputNums = new int[inputArr.length];
		//Initial arrays created, loop through and split each digit. If the 10th is an X, set to 10
		for (int i = 0; i < inputArr.length; i++) {
			if (inputArr[i].equals("X")) {
				inputNums[i] = 10;
			} else {
				try {
					inputNums[i] = Integer.parseInt(inputArr[i]);
				} catch (NumberFormatException e) {
					return false;
				}
			}
		}


		return ((inputNums[0] * 10 + inputNums[1] * 9 + inputNums[2] * 8 + inputNums[3] * 7 + inputNums[4] * 6
				+ inputNums[5] * 5 + inputNums[6] * 4 + inputNums[7] * 3 + inputNums[8] * 2 + inputNums[9] * 1) % 11) == 0;
	}

	/**
	 * 16. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 * 
	 * The quick brown fox jumps over the lazy dog.
	 * 
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isPangram(String string) {
		//Create an array to store the alphabet, and one for the input
		String[] alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
		String[] input = string.split("");
		//Need to keep track of the unique occurances of letters
//		boolean[] matches = new boolean[alphabet.length];
		int count = 0;

		//Loop through the alphabet first
		for (int a = 0; a < alphabet.length; a++) {
			//Now at each letter, scan the input for a match
			for (int i = 0; i < input.length; i++) {
				if (Arrays.asList(input).contains(alphabet[a])) {
					//set equal index to true when an alphabet character is found
//					matches[a] = true;
					count++;
					break;
				}

			}

		}
		return count == 26;
	}

	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given) {
		//if input is local date, convert to local date time at midnight
		LocalDateTime input;

		if (given instanceof LocalDate) {
			//Convert to DateTime if no time included
			input = ((LocalDate) given).atStartOfDay();
		} else {
			input = (LocalDateTime) given;
		}

		LocalDateTime output = input.plusSeconds(1000000000);
		return output;
	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param target
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int target, int[] set) {
		//Create starter variables
		List<Integer> multiples = new ArrayList<>();
		int current;
		//Loop that interates through the ints up until i, not including
		for (current = 0; current < target; current++) {
			//At each step, we now loop through set
			for (int m = 0; m < set.length; m++) {
				//Check for even division between current and m
				if (current%set[m]==0) {
					multiples.add(current);
					break;
				}
			}
		}
		int sum = 0;
		for (int c = 0; c < multiples.size(); c++) {
			sum += multiples.get(c);
		}

		return sum;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) {
		//Clean input
		//return false if there is a punctuation

		string = string.replaceAll(" ","");
		String[] input = string.split("");
		//any punctuation invalidates a number
		if (Arrays.asList(input).contains("-")) {
			return false;
		}

		List<Integer> numbers = new ArrayList<>();
		//add the int values of the input array to the numbers list
		for (String s : input) {
			try {
				numbers.add(Integer.parseInt(s));
			} catch (NumberFormatException n) {
				return false;
			}
		}

		int index;
		//Double every second digit starting from the end of the list
		for (int d = 2; d < numbers.size(); d+=2) {
			index = numbers.size()-d;

			numbers.set(index, numbers.get(index)*2);

			if (numbers.get(index)>9) {
				//If the new digit is > 9, subtract 9
				numbers.set(index, numbers.get(index)-9);
			}

		}
		//after doubling every second digit from right to left, sum all digits
		int sum = 0;
		for (Integer number : numbers) {
			sum += number;
		}
		return sum % 10 == 0;
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) {
		//First cleanse the input into usable array
		String problem = string.replaceAll("[?]","");

		String[] inputs = problem.split(" ");
		int firstNum = Integer.parseInt(inputs[2]);
		int secondNum = Integer.parseInt(inputs[inputs.length-1]);
		//Switch statement for checking the operation
		switch (inputs[3]) {
			case "plus":
				return firstNum+secondNum;

			case "minus":
				return firstNum-secondNum;

			case "multiplied":
				return firstNum*secondNum;

			case "divided":
				return firstNum/secondNum;

		}


		return 0;
	}

}
